This repository contains the architecture configuration that is deployed at [https://milbot-climat.unige.ch](https://milbot-climat.unige.ch).

The architecture is composed of
- a [Traefik instance](https://gitlab.unige.ch/cui/milbot/architecture/-/blob/master/reverse-proxy/docker-compose.yml?ref_type=heads#L3)
- the HTML/JS [Widget](https://gitlab.unige.ch/cui/milbot/architecture/-/blob/master/chatbot/docker-compose.yml?ref_type=heads#L3)
- the MILBOT chatbot developed with Rasa, therefore composed of
    - a Rasa [action server](https://gitlab.unige.ch/cui/milbot/architecture/-/blob/master/chatbot/docker-compose.yml?ref_type=heads#L21)
    - a Rasa [server](https://gitlab.unige.ch/cui/milbot/architecture/-/blob/master/chatbot/docker-compose.yml?ref_type=heads#L34)

